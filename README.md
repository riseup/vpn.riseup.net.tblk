# Tunnelblick Configuration

## What is this?

* The riseup VPN configuration files needed to run in OSX

## What you need?

* A computer running OSX also known as Macs
* Tunnelblick 3.7.0+. Get it from https://tunnelblick.net/downloads.html and install it.

## How to

1. Get the [configuration files](https://0xacab.org/riseup/vpn.riseup.net.tblk/repository/archive.zip?ref=master).
1. Uncompress them and double click vpn.riseup.net.tblk
